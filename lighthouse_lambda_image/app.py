
import requests
import subprocess
import json
import os


websites = {
    "river_island" : "https://www.riverisland.com/",
    "zara": "https://www.zara.com/uk/"
}

#Execute the bash subprocess
# Adding --no-sandbox chrome flag which I need for app to run
# Is max wait for load required?
def call_lighthouse(site,url):
    output_path = f'--output-path=./lighthouse-results_{site}.json'
    command = [
        'lighthouse',
        url,
        '"--chrome-flags=--no-sandbox --headless --disable-dev-shm-usage"',
        '--max-wait-for-load = 100000',
        #'--no-enable-error-reporting',
        '--output=json',
        output_path]
    try:
        subprocess.check_call(' '.join(command), shell=True)
    except subprocess.CalledProcessError as exc:
        msg = '''
        Command "{0}"
            returned an error code: {1},
            output: {2}
            '''.format(exc.cmd, exc.returncode, exc.clearoutput)
        raise RuntimeError(msg)



#Process the data to extract the json we need
def process_results(site,url):
    json_filepath = f'./lighthouse-results_{site}.json'
    #Import the file
    with open(json_filepath) as j:
        results = json.load(j)
    run_timestamp = results["fetchTime"]
    run_url = results["requestedUrl"]
    screen_emulation = results["configSettings"]["screenEmulation"]
    user_agent_emulation = results["configSettings"]["emulatedUserAgent"]
    #We can get all the numbers we need out of this section of the dict
    #print(results.keys())
    #print(results["audits"].keys())
    #print(results["audits"]["metrics"].keys())
    #print(results["audits"]["metrics"])
    #print(results["audits"]["metrics"]["details"].keys())
    lighthouse_scores = results["audits"]["metrics"]["details"]["items"][0]
    #Combine some of the metadata we need into a dict
    output_dict = {'run_timestamp': run_timestamp,
                'run_url': run_url,
                'user_agent_emulation': user_agent_emulation,
                'mobile_emulation': screen_emulation["mobile"],
                'screen_emulation_width': screen_emulation["width"],
                'screen_emulation_height': screen_emulation["height"],
                'screen_emulation_device_scale_factor': screen_emulation["deviceScaleFactor"]
                }
    #Append the performance scores
    output_dict.update(lighthouse_scores)

    #Convert keys to lowercase
    output_dict2 = {}
    for key in output_dict:
        low_key = key.lower()
        key_val = output_dict[key]
        output_dict2.update({low_key : key_val})
    os.remove(json_filepath)
    return output_dict2



# Main lambda function
def handler(event, context):
    webhook_url = "https://ri365.webhook.office.com/webhookb2/eb74e9b7-900f-460a-9cbf-7075389c435a@a3da1a5e-6af9-42be-8ad1-2a9b41101b90/IncomingWebhook/0282eeb7c1f54766912e70acf6cec3b8/ef5a0f8b-c21c-42e9-9934-7ef46307ff24"
    message = {"text" : "Hello World from inside a custom lambda runtime!!"}
    #Create a post request to send the message, formatting the data as json
    for site in websites:
        url = websites[site]
        #Call get lighthouse function
        print(f"Starting Lighthouse run for site: {url}...")
        call_lighthouse(site,url)
        print(f"Successfully collected results for site: {url}")
        print(f"Processing json outputs for site: {url}...")
        results_dict = process_results(site,url)
        print(f"Successfully processed outputs for site: {url}")
        #Load to dynamo db
        message2 = {"text" : results_dict}
        requests.post(webhook_url, json = message2)
    return 200
