# track_my_site_metrics
Lighthouse CLI Github can be found [here](#https://github.com/GoogleChrome/lighthouse#using-the-node-cli)

Lambda Docker base images can be found [here](https://docs.aws.amazon.com/lambda/latest/dg/images-create.html)

Collect, store, monitor and publish site performance metrics using Google's Lighthouse tool using a range of AWS services.

#### Custom Lambda Image based on the node.js image provided by AWS
- AWS Base images compatible with lambda can be found [here](https://docs.aws.amazon.com/lambda/latest/dg/nodejs-image.html)

- Build the container `docker build -t lambda_py .`
- Run the container `docker run -p 9000:8080 -d --name lighthouse_lambda lambda_py`

- Test the container locally
- https://docs.aws.amazon.com/lambda/latest/dg/images-test.html
- `curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}'`


To Do:
- Process JSON
- Load to Dynamo DB
- Push to ecr
- Invoke with lambda function and cloud watch trigger
