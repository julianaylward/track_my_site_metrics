#Put item (row) to existing dynamo table
#Sript adapted from here: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.03.html
#Note endpoint_url="http://localhost:8000" refers to local instance of dynamo rather than web which we are using

import boto3

def put_result(datetime, site, results, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb',  region_name='eu-west-2') 
    table = dynamodb.Table('lighthouse_results')
    response = table.put_item(
       Item={
            'datetime': datetime,
            'site': site,
            'results': results
        }
    )
    return response

# Where the KeySchema (datetime + site) matches, the put function will update, otherwise it will insert if no key exists

if __name__ == '__main__':
    put_resp = put_result(20210328, "river_island", 
                        { "First Contentful Paint": "2.15", "Largest Contentful Paint": "5.54"} 
                        )
    print("Put result succeeded:", put_resp)
