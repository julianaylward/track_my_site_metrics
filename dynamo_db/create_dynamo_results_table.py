# Adapted from AWS Getting Started Docs
# https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.01.html

import boto3


def create_results_table(dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb', region_name='eu-west-2')  #endpoint_url="http://localhost:8000")  for local dynamo
    table = dynamodb.create_table(
        TableName='lighthouse_results',
        KeySchema=[
            {'AttributeName': 'datetime', 'KeyType': 'HASH' }, #Partition Key
            {'AttributeName': 'site', 'KeyType': 'RANGE' } # Sort key
        ],
        AttributeDefinitions=[
            {'AttributeName': 'datetime', 'AttributeType': 'N'}, #Number
            {'AttributeName': 'site', 'AttributeType': 'S'}, # String
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10, 'WriteCapacityUnits': 10}
    )
    return table


if __name__ == '__main__':
    results_table = create_results_table()
    print("Table status:", results_table.table_status)

